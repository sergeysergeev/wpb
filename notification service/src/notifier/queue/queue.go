package queue

import (
	"log"
	"fmt"
	"github.com/streadway/amqp"
)

type RabbitEmailMessage struct {
	MessageId     string
	ConsumerEmail string
	Message       string
}

var Conn *amqp.Connection

var Ch *amqp.Channel

var Q amqp.Queue

//var EmailChan chan RabbitEmailMessage


func Init(){
	var err error
	Conn, err = amqp.Dial("amqp://guest:guest@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	Ch, err = Conn.Channel()
	failOnError(err, "Failed to open a channel")
	Q, err = Ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

}


func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}