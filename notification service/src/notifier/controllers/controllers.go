package controllers

import (
	"notifier/db"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	"notifier/queue"
	"github.com/streadway/amqp"
	"encoding/json"
	"fmt"
	"notifier/config"
)

type Event struct {
	Initiator string
	Service   string
	Event     string
}

func getRequestLogger(ctx *gin.Context) *log.Entry {
	requestId := config.GetIdRequest(ctx)
	return log.WithFields(log.Fields{"request_id": requestId})
}

func EventOccurred(context *gin.Context) {
	db.Db.LogMode(true)
	requestLogger := getRequestLogger(context)
	requestLogger.Info("event was occurred")
	var e Event
	err := context.BindJSON(&e)
	if err != nil {
		requestLogger.WithField("error_message", err.Error()).Error("JSON request binding error")
		context.JSON(http.StatusBadRequest, err)
		return
	}
	requestLogger.WithFields(
		log.Fields{
			"initiator": e.Initiator,
			"service":   e.Service,
			"event":     e.Event}).Debug("JSON request is bind to object Event successful")
	var watchers []db.Watcher
	db.Db.Where("event = ? and service = ?", e.Event, e.Service).Find(&watchers)

	for _, watcher := range watchers {
		message := queue.RabbitEmailMessage{
			MessageId:     config.GetIdRequest(context),
			ConsumerEmail: watcher.User,
			Message:       fmt.Sprintf("User %s create new event %s on service %s", e.Initiator, e.Event, e.Service),
		}
		byteMessage, err := json.Marshal(message)
		if err != nil {
			requestLogger.WithField("error_message", err.Error()).Error("Error at converting Message object to JSON")
			return
		}
		requestLogger.Debug("Message is converted successful")
		err = queue.Ch.Publish(
			"",           // exchange
			queue.Q.Name, // routing key
			false,        // mandatory
			false,        // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        byteMessage,
			})
		if err != nil {
			requestLogger.WithField("error_message", err.Error()).Error("Error at message publication to RabbitMQ")
			return
		}
	}
	requestLogger.Debug("Message is published to RabbitMQ")
	context.JSON(http.StatusAccepted, struct {
		Success bool
	}{true})

}

func GetWatcher(context *gin.Context) {
	requestLogger := getRequestLogger(context)
	requestLogger.WithFields(log.Fields{
		"user": context.Param("user"),
		"event": context.Param("event"),
		"service": context.Param("service"),
	}).Info("get watcher")
	requiredParams := []string{"user", "event", "service"}
	for _, val := range requiredParams {
		if context.Param(val) == "" {
			requestLogger.WithField("missed_param", val).Warn("request does not have a required parameter")
			context.JSON(http.StatusBadRequest, struct {
				Message string
			}{fmt.Sprintf("expected get param '%s' in request", val)})
			return
		}
	}
	var w db.Watcher
	err := db.Db.Where("user = %s and service = %s and event = %s",
		context.Param("user"),
		context.Param("service"),
		context.Param("event")).Find(&w).Error
	if err != nil {
		requestLogger.WithField("error_message", err.Error()).Error("Select watcher from DB error")
	}

	context.JSON(http.StatusOK, w)
}

func CreateNotification(context *gin.Context) {
	requestLogger := getRequestLogger(context)
	requestLogger.Info("create new watcher")
	var w db.Watcher
	err := context.BindJSON(&w)
	if err != nil {
		requestLogger.WithField("error_message", err.Error()).Error("JSON request binding error")
		context.JSON(http.StatusBadRequest, err)
		return
	}
	requestLogger.WithFields(
		log.Fields{
			"id": w.Id,
			"active": w.Active,
			"user":    w.User,
			"service": w.Service,
			"event":   w.Event}).Debug("JSON request is bind to object Event successful")
	if w.Id == 0 {
		requestLogger.Debug("create watcher")
		err = db.Db.Create(&w).Error
	} else {
		requestLogger.Debug("save watcher")
		err = db.Db.Save(&w).Error
	}

	if err != nil {
		requestLogger.WithField("error_message", err.Error()).Error("Creating new watcher error")
		context.JSON(http.StatusBadRequest, err)
		return
	}
	requestLogger.Debug("New watcher is created successful")
	context.JSON(http.StatusAccepted, struct {
		Success bool
	}{true})
}


func Authenticate() gin.HandlerFunc {
	return func(context *gin.Context) {
		requestLogger := getRequestLogger(context)
		requestLogger.Debug("auth middleware")
		authId := context.GetHeader("Auth")
		if !config.CheckIsValidClientId(authId) {
			requestLogger.Info("AUTH ERROR: invalid api key")
			context.JSON(http.StatusForbidden, struct {
				Status  string
				Message string
			}{"forbidden", "Invalid clientId"})
			context.Abort()
			return
		}
		requestLogger.Debug("checking in auth middleware is successful")
		context.Next()
	}
}
