package db

import (
	"github.com/jinzhu/gorm"
	"notifier/config"
	log "github.com/sirupsen/logrus"
)


type Watcher struct {
	Id int64 `gorm:"primary_key"`
	User  string `gorm:"unique_index:user_service_method_event_key"`
	Service string `gorm:"unique_index:user_service_method_event_key"`
	Event string `gorm:"unique_index:user_service_method_event_key"`
	Active bool
}
func (w Watcher) TableName() string  {
	return "watchers"
}

var Db *gorm.DB

func Init()  {
	log.Info("Start DB connect initialization")
	var err error
	Db, err = gorm.Open(config.GetDBDialect(), config.GetDBPath() )
	if err != nil {
		log.Fatal("Can't connect to DB")
		panic("MYSQL CONNECT PROBLEM")
	} else {
		log.Info("The connection DB was reached")
	}
	log.Info("Start DB migration")
	Db.AutoMigrate(&Watcher{})
	log.Info("DB migration is finished")
}


