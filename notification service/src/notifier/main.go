package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"notifier/db"
	"notifier/config"
	"notifier/controllers"
	"notifier/queue"
	log "github.com/sirupsen/logrus"
)


type ResponseObject struct {
	Status string
	Data interface{}
}


func main() {
	log.SetLevel(log.DebugLevel)
	log.Info("Notification Service is started")
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	config.Init()
	db.Init()
	queue.Init()
	defer queue.Conn.Close()
	defer db.Db.Close()

	auth := router.Group("/")
	auth.Use(controllers.Authenticate())
	auth.POST("/event", controllers.EventOccurred)
	auth.GET("/watch", controllers.GetWatcher)
	auth.POST("/watch", controllers.CreateNotification)
	auth.PUT("/watch", controllers.CreateNotification)

	if config.IsConnectByTls() {
		router.RunTLS(config.GetServerAddress(), config.GetSertFile(), config.GetKeyFile())
	}else {
		router.Run(config.GetServerAddress())
	}

}


