package com.wpb.auth.controller;

import com.wpb.auth.domain.AuthUser;
import com.wpb.auth.configuration.AuthUserPasswordEncoder;
import com.wpb.auth.repository.AuthUserRepository;
import com.wpb.auth.domain.UserAuthorities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Controller
@RefreshScope
public class AuthController {

    @Autowired
    AuthUserRepository authUserRepository;

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String registerForm(AuthUser authUser) {
        return "register";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public String registerNewUser(@Valid AuthUser authUser, BindingResult bind) {
        if (bind.hasErrors()) {
            return "register";
        }
        AuthUser user = authUserRepository.findByUsername(authUser.getUsername());
        if (user != null) {
//            bind.addError(new ObjectError("username", "An account already exists for this email."));
            bind.rejectValue("username", "username.reject.exist");
            return "register";
        }
        if (!authUser.getPassword().equals(authUser.getConfirmPassword())) {
            bind.rejectValue("password", "password.reject.same");
//            bind.addError(new ObjectError("password", "Fields 'Password' and 'Repeat password' must be same."));
            return "register";
        }
        authUser.setPassword(AuthUserPasswordEncoder.encode(authUser.getPassword()));

        UserAuthorities authorities = new UserAuthorities();
        authorities.setUsername(authUser.getUsername());
        authorities.setAuthority("ROLE_USER");

        Set<UserAuthorities> userAuthoritiesSet = new HashSet<>();
        userAuthoritiesSet.add(authorities);
        authUser.setEnabled(1);
        authUser.setAuthorities(userAuthoritiesSet);

        authUserRepository.save(authUser);

        return "success";
    }

    @Secured("hasRole('ADMIN')")
    @GetMapping("/users")
    public ModelAndView showUsers(){
        ModelAndView m = new ModelAndView();
        m.setViewName("users");
        m.addObject("users", authUserRepository.findAll());
        return m;
    }
}
