package com.wpb.auth.repository;

import com.wpb.auth.domain.UserAuthorities;
import org.springframework.data.repository.CrudRepository;

public interface UserAuthoritiesRepository  extends CrudRepository<UserAuthorities, String>{
}
