package com.wpb.auth.repository;

import com.wpb.auth.domain.AuthUser;
import org.springframework.data.repository.CrudRepository;


public interface AuthUserRepository extends CrudRepository<AuthUser, Integer> {

    AuthUser findByUsername(String username);

}
