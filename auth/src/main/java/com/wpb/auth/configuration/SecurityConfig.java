package com.wpb.auth.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/login", "/oauth/authorize", "/register", "/users")
                .and()
                .authorizeRequests()
                .antMatchers("/register")
                .anonymous()
                .and()
                .authorizeRequests()
                .antMatchers("/users")
                .hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .permitAll();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.parentAuthenticationManager(authenticationManager)
                .jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(AuthUserPasswordEncoder.getPasswordEncoder());
//                .withUser("admin@admin.ru")
//                .password(AuthUserPasswordEncoder.encode("1234"))
//                .roles("USER", "ADMIN")
//                .and().withUser("user@user.ru").password(AuthUserPasswordEncoder.encode("4321")).roles("USER");
    }

}
