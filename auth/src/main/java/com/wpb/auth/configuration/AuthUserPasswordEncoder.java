package com.wpb.auth.configuration;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class AuthUserPasswordEncoder {
    public static BCryptPasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    public static String encode(String password){
        return getPasswordEncoder().encode(password);
    }
}
