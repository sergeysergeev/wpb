package com.wpb.auth.domain;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;


@Entity
@Table(name = "users")
public class AuthUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Email
    @Column(unique = true)
    private String username;

    @Size(min = 4)
    @NotEmpty
    private String password;

    @Transient
    @NotEmpty
    private String confirmPassword;

    private Integer enabled;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<UserAuthorities> authorities;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Set<UserAuthorities> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<UserAuthorities> authorities) {
        this.authorities = authorities;
    }

}
