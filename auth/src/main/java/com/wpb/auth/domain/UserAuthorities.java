package com.wpb.auth.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "authorities")
public class UserAuthorities {

    @EmbeddedId
    UserAuthoritiesPK userAuthoritiesId;

    public UserAuthorities(){
        userAuthoritiesId = new UserAuthoritiesPK();
    }

    public String getUsername() {
        return this.userAuthoritiesId.getUsername();
    }

    public void setUsername(String username) {
        this.userAuthoritiesId.setUsername(username);
    }

    public String getAuthority() {
        return this.userAuthoritiesId.getAuthority();
    }

    public void setAuthority(String authority) {
        this.userAuthoritiesId.setAuthority(authority);
    }
}

@Embeddable
class UserAuthoritiesPK implements Serializable {

    private String username;

    private String authority;

    public UserAuthoritiesPK(){

    }

    public UserAuthoritiesPK(String username, String authority){
        this.username = username;
        this.authority = authority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}