package com.wpb.authclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.UUID;

public class ClientHttpInterceptor implements ClientHttpRequestInterceptor {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private String clientId;

    public ClientHttpInterceptor(String cId){
        this.clientId = cId;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
        HttpHeaders headers =  httpRequest.getHeaders();
        headers.add("Auth", clientId);
        UUID requestId = UUID.randomUUID();
        headers.add("Request-Id", "EVENT_UI-" + requestId.toString());
        log.info("Prepare request with requestID = " + requestId.toString());
        return clientHttpRequestExecution.execute(httpRequest, bytes);
    }
}
