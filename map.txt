digraph G {
 ratio = fill;
 node [style=filled];
  "Gateway" -> "SSO" [color="0.002 0.999 0.999"];
//  "Task Service" -> "SSO";
  "Event Service" -> "SSO";
//  "Gateway" -> "Task Service";
  "Gateway" -> "Event Service";
//  "Task Service" -> "Task microservice";
  "Event Service" -> "Event microservice";
  "Event microservice" -> "Notification service";
//  "Task microservice" -> "Notification service";
 // "Notification service" -> "Sender";
///test;
  "SSO" -> "SSO DB";
  "SSO" -> "Redis";
  "SSO" -> "sso.log";
//  "Task microservice" -> "Task DB";
//  "Task microservice" -> "task.log";
  "Event microservice" -> "Event DB";
  "Event microservice" -> "event.log";
  "Notification service" -> "Queue";
  "Queue" -> "Sender";
  "Sender" -> "external mail server (SMTP)";
  "Sender" -> "sender.log";

  "Sender" [color="0.002 0.999 0.999"];

}
