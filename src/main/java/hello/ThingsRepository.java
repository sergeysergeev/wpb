package hello;

import org.springframework.data.repository.CrudRepository;

public interface ThingsRepository extends CrudRepository<Things, Integer> {
}
