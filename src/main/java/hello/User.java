package hello;

import javax.persistence.*;
import java.util.Set;


public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String login;

    @Column(unique = true)
    private String email;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Things> things;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Things> getThings() {
        return things;
    }

    public void setThings(Set<Things> things) {
        this.things = things;
    }
}
