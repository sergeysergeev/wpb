package hello;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RefreshScope
public class PingController {


    @Value("${info.foo}")
    private String fooProperty;

    @RequestMapping(path = "/ping", method = GET)
    public String ping(){
        return "{\"status\":true, \"message\": \""+ fooProperty + "\"}";
    }

}
