package main

import ("github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"log"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"crypto/md5"
	"io"
)


type CurrentUser struct {
	Authenticated bool
	UserAuthentication UserAuthentication
}

type UserAuthentication struct {
	Principal Principal
}

type Principal struct {
	Username string
	Authorities []Authorities

}

type Authorities struct {
	Authority string
}

var sessions map[string] *oauth2.Token

func main(){

	sessions = make(map[string] *oauth2.Token)
	router := gin.Default()

	conf := &oauth2.Config{
		ClientID:     "fooClientIdPassword",
		ClientSecret: "secret",
		Scopes:       []string{"user_info"},
		Endpoint: oauth2.Endpoint{
			TokenURL: "http://localhost:8081/auth/oauth/token",
			AuthURL:  "http://localhost:8081/auth/oauth/authorize",
		},
		RedirectURL: "http://localhost:8080/login",
	}

	url := conf.AuthCodeURL("state", oauth2.AccessTypeOnline)

	router.GET("/secure", func(context *gin.Context) {
		val, err := context.Cookie("sessionId")
		if err != nil {
			context.Writer.Write([]byte("Invalid session"))
			return
		}

		tok, isExist := sessions[val]
		if !isExist {
			context.Writer.Write([]byte("Invalid session"))
			return
		}
		client := conf.Client(context, tok)
		resp, err := client.Get("http://localhost:8081/auth/user/me")
		if err != nil{
			log.Println(err)
		}
		defer resp.Body.Close()
		responseBody, err := ioutil.ReadAll(resp.Body)
		log.Println("body ", string(responseBody))
		context.Writer.Write(responseBody)

	})


	router.GET("/test", func(context *gin.Context) {
		context.Redirect(301, url)
	})
	router.GET("/login", func(context *gin.Context) {
		log.Println(context.Query("code"))
		code := context.Query("code")
		if code != "" {
			tok, err := conf.Exchange(context, code)
			if err != nil {
				log.Fatal(err)
			}
			log.Println(fmt.Sprintf("Take token: %s", tok))
			h := md5.New()
			io.WriteString(h, tok.AccessToken)
			sessionId := string(h.Sum(nil))
			sessions[sessionId] = tok
			context.SetCookie("sessionId", sessionId, 1, "/", "localhost", true, true)

			client := conf.Client(context, tok)
			resp, err := client.Get("http://localhost:8081/auth/user/me")
			if err != nil{
				log.Println(err)
			}
			defer resp.Body.Close()
			responseBody, err := ioutil.ReadAll(resp.Body)
			log.Println("body ", string(responseBody))
			var currentUser CurrentUser
			err = json.Unmarshal(responseBody, &currentUser)
			if err != nil {
				context.Writer.WriteString("Error" + fmt.Sprint(err))
			}
			context.Writer.Write([]byte(fmt.Sprintf("Username: %s %v", currentUser.UserAuthentication.Principal.Username, currentUser)))
		}

	})

	router.Run(":8080")

}
