package com.wpb.authclient.controller;


import com.wpb.authclient.ClientHttpInterceptor;
import com.wpb.authclient.domain.Event;
import com.wpb.authclient.domain.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

@Controller
@RefreshScope
public class EventController {
    @Value("${custom.api-keys.event}")
    String eventClientId;
    @Value("${custom.api-keys.notify}")
    String notifyClientId;


    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/events/add")
    public String registerForm(Event event) {
        return "eventsAdd";
    }

    @GetMapping("/events/{id}")
    public String getEvent(@PathVariable("id") Integer id, Model model){
        RestTemplate tmpl = new RestTemplate();
        tmpl.setInterceptors(Collections.singletonList(new ClientHttpInterceptor(this.eventClientId)));
        ResponseEntity<Event> e = tmpl.getForEntity("http://localhost:8080/events/{id}", Event.class, id);
        model.addAttribute("event", e.getBody());
        return "event";
    }

    @PostMapping("events")
    public String postEvent(@Valid Event e, BindingResult bind){
        log.info("event save method request Id={} title={} startDate={} finishDate={} active={}  ",
                e.getId(),
                e.getTitle(),
                e.getStartDate(),
                e.getFinishDate(),
                e.getActive());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        e.setActive(true);
        e.setUser(auth.getName());
        RestTemplate tmpl = new RestTemplate();
        tmpl.setInterceptors(Collections.singletonList(new ClientHttpInterceptor(this.eventClientId)));
        ResponseEntity<Event> e1 = tmpl.postForEntity("http://localhost:8080/events", e, Event.class);
        return "events";
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @GetMapping("events")
    public String getEvents(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        RestTemplate tmpl = new RestTemplate();
        tmpl.setInterceptors(Collections.singletonList(new ClientHttpInterceptor(this.eventClientId)));
        ResponseEntity<Event[]> e1 = tmpl.getForEntity("http://localhost:8080/events?user="+auth.getName(), Event[].class);
        model.addAttribute("events", e1.getBody());
        return "events";
    }

    @GetMapping("watch")
    public RedirectView watch(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        RestTemplate tmpl = new RestTemplate();
        Watcher w = new Watcher();
        w.event = "add_new_event_by_user_" + auth.getName();
        w.active = true;
        w.service = "event_ui";
        w.user = auth.getName();
        tmpl.setInterceptors(Collections.singletonList(new ClientHttpInterceptor(this.notifyClientId)));
        ResponseEntity<Watcher> w1 = tmpl.postForEntity("http://localhost:8181/watch", w, Watcher.class);
        return new RedirectView("/events");
    }



}
