package com.wpb.authclient.repository;

import com.wpb.authclient.domain.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Integer> {
    @Override
    Event findOne(Integer integer);

    @Override
    void delete(Event event);

    @Override
    void delete(Integer integer);
}
