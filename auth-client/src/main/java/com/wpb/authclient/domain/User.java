package com.wpb.authclient.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "client_users")
public class User {
    @Id
    private Integer id;

    private String username;

}
