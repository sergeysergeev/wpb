package com.wpb.authclient.domain;

public class Watcher {

    /*
    Id int64 `gorm:"primary_key"`
	User  string `gorm:"unique_index:user_service_method_event_key"`
	Service string `gorm:"unique_index:user_service_method_event_key"`
	Event string `gorm:"unique_index:user_service_method_event_key"`
	Active bool
     */

    public Integer id;
    public String user;
    public String service;
    public String event;
    public boolean active;
}
