package main

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"net/smtp"
	"sender/config"
)

type RabbitEmailMessage struct {
	MessageId     string
	ConsumerEmail string
	Message       string
}

func main() {
	config.Init()

	conn, err := amqp.Dial(config.GetAmqpUrl())
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello",
		//config.GetQueueName(), // name
		false,   // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	host := "smtp.gmail.com"
	port := 587
	auth := smtp.PlainAuth("", "test.diploma124@gmail.com", "abc623950", host)

	go func() {
		for d := range msgs {
			var message RabbitEmailMessage
			err := json.Unmarshal(d.Body, &message)
			if err != nil {
				log.Println("[ERROR] can't marshal ", string(d.Body))
				continue
			}
			err = smtp.SendMail(fmt.Sprintf("%s:%d", host, port), auth, "test.diploma124@gmail.com", []string{message.ConsumerEmail}, []byte(message.Message))
			if err != nil {
				log.Printf("[ERROR] [messageId = %s] can't send email", message.MessageId)
			} else {
				log.Printf("[INFO] [messageId = %s] message was send", message.MessageId)
			}
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")

	<-forever
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("[FATAL] %s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}
