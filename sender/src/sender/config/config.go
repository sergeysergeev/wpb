package config

import (
	"io/ioutil"
	"fmt"
	"gopkg.in/yaml.v2"
	log "github.com/sirupsen/logrus"
)

var Cnfg AllConfig

type AllConfig struct {
	Database DatabaseConfig
	Clients  []string
	Server   ServerConfig
	Rabbit   RabbitConfig
}

type DatabaseConfig struct {
	Dialect  string
	User     string
	Password string
	Name     string
	Host     string
	Port     int
}

type ServerConfig struct {
	Tls      bool
	Port     int
	CertFile string
	KeyFile  string
}

type RabbitConfig struct {
	User      string
	Password  string
	Host      string
	Port      int
	QueueName string
}

func Init() {
	log.Info("Start Config Initialization")
	dataByte, err := ioutil.ReadFile("config/config.yml")
	if err != nil {
		log.Fatal("Can't read config file")
		panic(err)
	}
	err = yaml.Unmarshal(dataByte, &Cnfg)
	if err != nil {
		log.Fatal("Can't read config file")
		panic(err)
	}
	log.Info("Config was initialized")
}

func GetDBPath() string {
	test := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		Cnfg.Database.User,
		Cnfg.Database.Password,
		Cnfg.Database.Host,
		Cnfg.Database.Port,
		Cnfg.Database.Name)
	return test
}

func GetDBDialect() string {
	return Cnfg.Database.Dialect
}

func IsConnectByTls() bool {
	return Cnfg.Server.Tls
}

func GetServerAddress() string {
	return fmt.Sprintf(":%d", Cnfg.Server.Port)
}

func GetSertFile() string {
	return Cnfg.Server.CertFile
}

func GetKeyFile() string {
	return Cnfg.Server.KeyFile
}

func GetAmqpUrl() string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d/",
		Cnfg.Rabbit.User,
		Cnfg.Rabbit.Password,
		Cnfg.Rabbit.Host,
		Cnfg.Rabbit.Port)
}

func GetQueueName() string {
	return Cnfg.Rabbit.QueueName
}

func CheckIsValidClientId(clientId string) bool {
	for _, validClientId := range Cnfg.Clients {
		if clientId == validClientId {
			return true
		}
	}
	return false
}
