package com.wpb.eventmicroservice;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "custom.api-keys")
public class ApiKeys {
    private List<String> server ;
    ApiKeys() {
        this.server = new ArrayList<>();
    }

    public List<String> getServer() {
        return server;
    }
}
