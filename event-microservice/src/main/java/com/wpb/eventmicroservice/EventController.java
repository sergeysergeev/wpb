package com.wpb.eventmicroservice;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.wpb.eventmicroservice.repository.EventRepository;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@RestController
public class EventController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public EventRepository eventRepository;

    @GetMapping("/events/{id}")
    public Event getEvent(@PathVariable("id") Integer id, HttpServletRequest request){
        log.info("Get event with id={}; RequestId={}", id, request.getHeader("RequestId"));
        return eventRepository.findOne(id);
    }

    @GetMapping("/events")
    public List<Event> getEvents (@RequestParam("user") String email, HttpServletRequest request){
        log.info("Get event with user={}; RequestId={}", email , request.getHeader("RequestId"));
        return eventRepository.findByUser(email);
    }

    @RequestMapping(value = "/events", method = {RequestMethod.POST, RequestMethod.PUT})
    public Event createEvent(@RequestBody Event event, HttpServletRequest request){
        log.info("event save method request Id={} title={} startDate={} finishDate={} active={}; RequestId={} ",
                event.getId(),
                event.getTitle(),
                event.getStartDate(),
                event.getFinishDate(),
                event.getActive(),
                request.getHeader("RequestId")
                );

        eventRepository.save(event);
        RestTemplate tmpl = new RestTemplate();
        NotifyEvent ne = new NotifyEvent();
        ne.Initiator = event.getUser();
        ne.Event = "add_new_event_by_user_" + event.getUser();
        ne.Service = "event_ui";
        tmpl.setInterceptors(Collections.singletonList(new DeprecatedClientHttpInterceptor("54b60cef-6460-4aa4-9284-6a6d40f4af8a")));
        ResponseEntity<NotifyEvent> ne1 = tmpl.postForEntity("http://localhost:8181/event", ne, NotifyEvent.class);

        return event;
    }

    @DeleteMapping("/events/{id}")
    public Boolean deleteEvent(@RequestParam Integer id){
        eventRepository.delete(id);
        return true;
    }

}
