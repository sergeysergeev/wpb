package com.wpb.eventmicroservice.repository;

import com.wpb.eventmicroservice.Event;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface EventRepository extends CrudRepository<Event, Integer> {
    @Override
    Event findOne(Integer integer);

    @Override
    void delete(Integer integer);

    List<Event> findByUser(String email);
}
